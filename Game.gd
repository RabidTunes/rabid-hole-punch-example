extends Node2D

onready var out = $Out
onready var p1 = $Player1
onready var p2 = $Player2
onready var p3 = $Player3
onready var p4 = $Player4
onready var players = [p1, p2, p3, p4]
onready var players_init_pos = [p1.position, p2.position, p3.position, p4.position]


func _ready() -> void:
	for player in players:
		player.set_network_master(0)


func update_player(index: int, peer_id: int, player_name: String) -> void:
	var player = players[index]
	player.set_name(player_name)
	player.set_network_master(peer_id)
	if peer_id == 0:
		player.position = out.position
	else:
		if player.position.x < 0:
			player.position = players_init_pos[index]
