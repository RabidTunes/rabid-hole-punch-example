extends Control


onready var label: Label = $Panel/Label


func change_message(message: String):
	label.text = message

func reset_message():
	label.text = "LOADING"
