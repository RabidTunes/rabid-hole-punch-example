extends Node

onready var connection_menu: Control = $ConnectionMenu
onready var session_menu: Control = $SessionMenu
onready var loading_panel: Control = $LoadingPanel
onready var error_dialog: AcceptDialog = $ErrorDialog

onready var game = preload("res://Game.tscn")
var game_instance
var player_name: String

var players: Array = []


func _ready() -> void:
	RabidHolePuncher.connect("holepunch_success", self, "_on_holepunch_success")
	RabidHolePuncher.connect("holepunch_progress_update", self, "_on_holepunch_progress_update")
	RabidHolePuncher.connect("holepunch_failure", self, "_on_holepunch_failure")

func _on_holepunch_success(self_port, host_ip, host_port) -> void:
	print("Holepunch is success")
	print(self_port)
	print(host_ip)
	print(host_port)
	if not host_ip:
		# We are host
		print("Creating server")
		var net = NetworkedMultiplayerENet.new()
		net.connect("peer_connected", self, "_on_peer_connected")
		net.create_server(self_port, 4)
		get_tree().set_network_peer(net)
		var own_data = [1, RabidHolePuncher.get_player_name()]
		players = [own_data]
	else:
		print("Creating client")
		var net = NetworkedMultiplayerENet.new()
		net.create_client(host_ip, host_port, 0, 0, self_port)
		get_tree().set_network_peer(net)
		loading_panel.change_message("Connecting to host...")
	game_instance = game.instance()
	player_name = RabidHolePuncher.get_player_name()
	for child in get_children():
		child.queue_free()
	add_child(game_instance)


func _on_peer_connected(peer):
	rpc_id(peer, "get_player_name")


remote func get_player_name():
	var sender = get_tree().get_rpc_sender_id()
	rpc_id(sender, "return_player_name", player_name)


remote func return_player_name(player_name: String) -> void:
	var sender = get_tree().get_rpc_sender_id()
	players.append([sender, player_name])
	rpc("update_players", players)


remotesync func update_players(players: Array) -> void:
	for i in range(4):
		if i >= players.size():
			game_instance.update_player(i, 0, "")
		else:
			var player_data = players[i]
			game_instance.update_player(i, player_data[0], player_data[1])


func _on_holepunch_progress_update(type, session_name, player_names) -> void:
	if type == RabidHolePuncher.STATUS_SESSION_CREATED or type == RabidHolePuncher.STATUS_SESSION_UPDATED:
		_on_session_updated(session_name, player_names)
	elif type == RabidHolePuncher.STATUS_STARTING_SESSION and not RabidHolePuncher.is_host():
		session_menu.hide()
		loading_panel.show()
		loading_panel.change_message("Starting session...")
	elif type == RabidHolePuncher.STATUS_SENDING_GREETINGS:
		loading_panel.change_message("Establishing self address...")
	elif type == RabidHolePuncher.STATUS_SENDING_CONFIRMATIONS:
		loading_panel.change_message("Confirming peers' addresses...")


func _on_session_updated(session_name, player_names) -> void:
	loading_panel.hide()
	session_menu.show()
	session_menu.update_players_list(player_names)
	if player_names[0] == RabidHolePuncher.get_player_name():
		session_menu.set_host(true)
	else:
		session_menu.set_host(false)


func _on_holepunch_failure(error) -> void:
	session_menu.hide()
	loading_panel.hide()
	loading_panel.reset_message()
	connection_menu.show()
	error_dialog.dialog_text = error
	error_dialog.popup()


func _on_ConnectionMenu_connection_called(is_host, player_name, session_name, session_pass) -> void:
	if is_host:
		RabidHolePuncher.create_session(session_name, player_name, 4, session_pass)
	else:
		RabidHolePuncher.join_session(session_name, player_name, 4, session_pass)
	connection_menu.hide()
	loading_panel.show()


func _on_SessionMenu_kicked_player(player_name) -> void:
	RabidHolePuncher.kick_player(player_name)


func _on_SessionMenu_start() -> void:
	RabidHolePuncher.start_session()
	session_menu.hide()
	loading_panel.show()
	loading_panel.change_message("Starting session...")


func _on_SessionMenu_exit() -> void:
	RabidHolePuncher.exit_session()
	session_menu.hide()
	loading_panel.show()
