extends KinematicBody2D

const PLAYER_COLORS = [Color.cornflower, Color.crimson, Color.darkorange, Color.greenyellow]

export var player_number: int = 1
export var player_name: String = "Player"

onready var label: Label = $Label

var direction = Vector2()
var speed = 200

func _ready() -> void:
	$Sprite.modulate = PLAYER_COLORS[(player_number - 1) % PLAYER_COLORS.size()]
	$Label.text = player_name


remote func _set_position(pos):
	global_transform.origin = pos


func _physics_process(delta):
	if is_network_master():
		var axis = Vector2.ZERO
		axis.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
		axis.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
		move_and_slide(axis*speed)
		rpc_unreliable("_set_position", global_transform.origin)


func set_name(player_name: String) -> void:
	label.text = player_name
