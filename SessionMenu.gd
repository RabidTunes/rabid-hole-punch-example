extends Control

signal start
signal exit
signal kicked_player(player_name)


onready var player_row_scene = preload("res://PlayerRow.tscn")
onready var players_container: Control = $Panel/VBoxContainer/PlayersContainer
onready var start_session_button: Button = $Panel/VBoxContainer/VBoxContainer2/HBoxContainer/StartSession


func update_players_list(players: Array) -> void:
	for child in players_container.get_children():
		if "Row" in child.name:
			if players.empty():
				child.queue_free()
			else:
				var player_name: String = players.pop_front()
				child.set_player_name(player_name)
	
	for player_name in players:
		var player_row_instance = player_row_scene.instance()
		player_row_instance.connect("kicked_player", self, "_on_player_row_kicked")
		players_container.call_deferred("add_child", player_row_instance)
		yield(player_row_instance, "ready")
		player_row_instance.set_player_name(player_name)


func set_host(host: bool) -> void:
	if host:
		start_session_button.show()
	else:
		start_session_button.hide()
	for child in players_container.get_children():
		if "Row" in child.name:
			child.set_host(host)


func _on_StartSession_pressed() -> void:
	var count = 0
	for child in players_container.get_children():
		if "Row" in child.name:
			count += 1
	if count > 1:
		emit_signal("start")
		hide()


func _on_player_row_kicked(player_name: String) -> void:
	emit_signal("kicked_player", player_name)


func _on_LeaveSession_pressed() -> void:
	emit_signal("exit")
	hide()
