extends Control

signal connection_called(is_host, player_name, session_name, session_pass)

onready var player_name: LineEdit = $Panel/VBoxContainer/PlayerNameInput
onready var session_name: LineEdit = $Panel/VBoxContainer/SessionNameInput
onready var session_pass: LineEdit = $Panel/VBoxContainer/SessionPassInput

onready var host_button: Button = $Panel/VBoxContainer/HBoxContainer/HostButton
onready var join_button: Button = $Panel/VBoxContainer/HBoxContainer/JoinButton


func _on_HostButton_pressed() -> void:
	_emit_connection_called(true)


func _on_JoinButton_pressed() -> void:
	_emit_connection_called(false)


func _emit_connection_called(is_host: bool) -> void:
	emit_signal("connection_called", is_host, player_name.text, session_name.text, session_pass.text)


func enable_buttons() -> void:
	host_button.disabled = false
	join_button.disabled  = false


func disable_buttons() -> void:
	host_button.disabled = true
	join_button.disabled  = true
