# Rabid Hole Punch Example

Example Godot project to demonstrate how it works

Before testing you have to modify `res://addons/rabidholepuncher/RabidHolePunch.tscn`, specifically the variables `RelayServerAddress` and `RelayServerPort` to match IP and port of your public [Rabid Hole Punch Server](https://gitlab.com/RabidTunes/rabid-hole-punch-server), otherwise it won't work

[You can find the standalone Godot plugin here](https://gitlab.com/RabidTunes/rabid-hole-punch-godot)
