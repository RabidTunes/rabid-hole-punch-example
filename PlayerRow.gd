extends HBoxContainer

signal kicked_player(player_name)


onready var player_name_label: Label = $PlayerName
onready var kick_button: Button = $KickButton


func _ready() -> void:
	set_host(RabidHolePuncher.is_host())


func set_player_name(player_name: String) -> void:
	player_name_label.text = player_name


func get_player_name() -> String:
	return player_name_label.text


func _on_KickButton_pressed() -> void:
	emit_signal("kicked_player", self.get_player_name())


func set_host(host: bool) -> void:
	if host:
		kick_button.show()
	else:
		kick_button.hide()
